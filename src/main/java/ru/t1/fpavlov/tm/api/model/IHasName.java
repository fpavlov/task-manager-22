package ru.t1.fpavlov.tm.api.model;

/**
 * Created by fpavlov on 26.11.2021.
 */
public interface IHasName {

    String getName();

    void setName(final String name);

}
