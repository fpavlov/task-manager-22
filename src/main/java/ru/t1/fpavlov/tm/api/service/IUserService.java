package ru.t1.fpavlov.tm.api.service;

import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.model.User;

/**
 * Created by fpavlov on 20.12.2021.
 */
public interface IUserService extends IService<User> {

    User create(final String login, final String password);

    User create(final String login, final String password, final String email);

    User create(final String login, final String password, final Role role);

    User findByLogin(final String login);

    User findByEmail(final String email);

    User removeByLogin(final String login);

    User removeByEmail(final String email);

    User setPassword(final String id, final String password);

    User updateUser(final String id,
                    final String firstName,
                    final String lastName,
                    final String middleName);

    Boolean isLoginExist(final String login);

    Boolean isEmailExist(final String email);

    void lockUserByLogin(final String login);

    void unlockUserByLogin(final String login);

}
