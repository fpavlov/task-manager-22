package ru.t1.fpavlov.tm.api.service;

import ru.t1.fpavlov.tm.model.Project;

/**
 * Created by fpavlov on 26.11.2021.
 */
public interface IProjectTaskService {

    void bindTaskToProject(final String userId, final String projectId, final String taskId);

    void removeProject(final String userId, final Project project);

    void removeProjectById(final String userId, final String projectId);

    void unbindTaskToProject(final String userId, final String projectId, final String taskId);

}
