package ru.t1.fpavlov.tm.api.service;

import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Project;

/**
 * Created by fpavlov on 10.10.2021.
 */
public interface IProjectService extends IUserOwnedService<Project> {

    Project create(final String userId, final String name);

    Project create(final String userId, final String name, final String description);

    Project changeStatus(final String userId, final Project project, final Status status);

    Project changeStatusById(final String userId, final String id, final Status status);

    Project changeStatusByIndex(final String userId, final Integer index, final Status status);

    Project update(final String userId, final Project project, final String name, final String description);

    Project updateById(final String userId, final String id, final String name, final String description);

    Project updateByIndex(final String userId, final Integer index, final String name, final String description);

}
