package ru.t1.fpavlov.tm.exception.user;

/**
 * Created by fpavlov on 24.01.2022.
 */
public final class AuthenticationException extends AbstractUserException {

    public AuthenticationException() {
        super("Error! Authentication is fallen, your account is lock");
    }

}
