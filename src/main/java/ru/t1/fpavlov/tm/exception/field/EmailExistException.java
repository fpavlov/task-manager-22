package ru.t1.fpavlov.tm.exception.field;

/**
 * Created by fpavlov on 20.12.2021.
 */
public final class EmailExistException extends AbstractFieldException {

    public EmailExistException() {
        super("Error! This e-mail already exists");
    }

}
