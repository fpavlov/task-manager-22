package ru.t1.fpavlov.tm.command.user;

import ru.t1.fpavlov.tm.enumerated.Role;

/**
 * Created by fpavlov on 24.01.2022.
 */
public final class UserLockCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Lock user";

    public static final String NAME = "user-lock";

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIB};
    }

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final String login = this.input("Login:");
        this.getUserService().lockUserByLogin(login);
    }

}
