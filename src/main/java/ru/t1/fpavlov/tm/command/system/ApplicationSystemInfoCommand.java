package ru.t1.fpavlov.tm.command.system;

import static ru.t1.fpavlov.tm.util.FormatUtil.bytesToHumanReadable;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ApplicationSystemInfoCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-i";

    public static final String DESCRIPTION = "Display some information about system";

    public static final String NAME = "info";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final Runtime runtime = Runtime.getRuntime();
        System.out.format(
                "System info:%n" +
                        "\t - Available processors: %s%n" +
                        "\t - Free memory: %s%n" +
                        "\t - Maximum memory: %s%n" +
                        "\t - Total memory: %s%n" +
                        "\t - Used memory: %s%n",
                runtime.availableProcessors(),
                bytesToHumanReadable(runtime.freeMemory()),
                bytesToHumanReadable(runtime.maxMemory()),
                bytesToHumanReadable(runtime.totalMemory()),
                bytesToHumanReadable(runtime.totalMemory() - runtime.freeMemory())
        );
    }

}
