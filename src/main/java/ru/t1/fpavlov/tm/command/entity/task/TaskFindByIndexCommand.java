package ru.t1.fpavlov.tm.command.entity.task;

import ru.t1.fpavlov.tm.model.Task;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskFindByIndexCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Find Task by index and then display it";

    public static final String NAME = "task-find-by-index";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final Task entity = this.findByIndex();
        System.out.println(entity);
    }

}
