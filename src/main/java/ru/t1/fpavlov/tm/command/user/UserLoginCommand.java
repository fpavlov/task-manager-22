package ru.t1.fpavlov.tm.command.user;

import ru.t1.fpavlov.tm.enumerated.Role;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class UserLoginCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Login";

    public static final String NAME = "login";

    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final String login = this.input("Login:");
        final String password = this.input("Password:");
        this.getAuthService().login(login, password);
    }

}
