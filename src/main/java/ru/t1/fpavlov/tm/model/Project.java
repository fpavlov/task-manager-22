package ru.t1.fpavlov.tm.model;

import ru.t1.fpavlov.tm.api.model.IWBS;
import ru.t1.fpavlov.tm.enumerated.Status;

import java.util.Date;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class Project extends AbstractUserOwnedModel implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    public Project() {
    }

    public Project(final String name) {
        this.name = name;
    }

    public Project(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public Status getStatus() {
        return this.status;
    }

    @Override
    public void setCreated(final Date created) {
        this.created = created;
    }

    @Override
    public Date getCreated() {
        return this.created;
    }

    @Override
    public String toString() {
        return "".format(" |%40s |%10s |%20s |%20s |%20s |",
                this.getId(),
                this.created,
                this.name,
                this.description,
                Status.toName(this.status));
    }

}
