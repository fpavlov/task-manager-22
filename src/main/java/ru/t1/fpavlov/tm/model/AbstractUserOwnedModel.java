package ru.t1.fpavlov.tm.model;

/**
 * Created by fpavlov on 13.01.2022.
 */
public abstract class AbstractUserOwnedModel extends AbstractModel {

    protected String userId;

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

}
