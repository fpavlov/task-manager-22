package ru.t1.fpavlov.tm.service;

import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.api.repository.IUserRepository;
import ru.t1.fpavlov.tm.api.service.IUserService;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.exception.entity.UserNotFoundException;
import ru.t1.fpavlov.tm.exception.field.EmailEmptyException;
import ru.t1.fpavlov.tm.exception.field.EmailExistException;
import ru.t1.fpavlov.tm.exception.field.IdEmptyException;
import ru.t1.fpavlov.tm.exception.field.PasswordEmptyException;
import ru.t1.fpavlov.tm.exception.user.LoginEmptyException;
import ru.t1.fpavlov.tm.exception.user.LoginExistException;
import ru.t1.fpavlov.tm.exception.user.RoleEmptyException;
import ru.t1.fpavlov.tm.model.User;
import ru.t1.fpavlov.tm.util.HashUtil;

/**
 * Created by fpavlov on 20.12.2021.
 */
public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public UserService(
            final IUserRepository repository,
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        super(repository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (this.isLoginExist(login)) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return this.repository.create(login, password);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (this.isLoginExist(login)) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (this.isEmailExist(email)) throw new EmailExistException();
        return this.repository.create(login, password, email);
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (this.isLoginExist(login)) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        return this.repository.create(login, password, role);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return this.repository.findByLogin(login);
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return this.repository.findByEmail(email);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = this.findByLogin(login);
        this.taskRepository.clear(user.getId());
        this.projectRepository.clear(user.getId());
        return this.remove(user);
    }

    @Override
    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = this.findByEmail(email);
        return this.remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = this.findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(final String id,
                           final String firstName,
                           final String lastName,
                           final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = this.findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return this.repository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return this.repository.isEmailExist(email);
    }

    @Override
    public void lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = this.findByLogin(login);
        user.setIsLocked(true);
    }

    @Override
    public void unlockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = this.findByLogin(login);
        user.setIsLocked(false);
    }

}
