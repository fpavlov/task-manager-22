package ru.t1.fpavlov.tm.service;


import ru.t1.fpavlov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

/**
 * Created by fpavlov on 07.12.2021.
 */
public final class LoggerService implements ILoggerService {

    private static final String CONFIG_FILE = "/logger.properties";

    private static final String COMMANDS = "COMMANDS";

    private static final String COMMANDS_FILE = "./commands.xml";

    private static final String ERRORS = "ERRORS";

    private static final String ERRORS_FILE = "./errors.xml";

    private static final String MESSAGES = "MESSAGES";

    private static final String MESSAGES_FILE = "./messages.xml";

    private static final LogManager MANAGER = LogManager.getLogManager();

    private static final Logger ROOT = Logger.getLogger("");

    private static final Logger COMMANDS_LOGGER = getLoggerCommand();

    private static final Logger ERRORS_LOGGER = getLoggerError();

    private static final Logger MESSAGES_LOGGER = getLoggerMessage();

    private final ConsoleHandler consoleHandler = getConsoleHandler();

    public static Logger getLoggerCommand() {
        return Logger.getLogger(COMMANDS);
    }

    public static Logger getLoggerError() {
        return Logger.getLogger(ERRORS);
    }

    public static Logger getLoggerMessage() {
        return Logger.getLogger(MESSAGES);
    }

    {
        init();
        registry(COMMANDS_LOGGER, COMMANDS_FILE, false);
        registry(ERRORS_LOGGER, ERRORS_FILE, true);
        registry(MESSAGES_LOGGER, MESSAGES_FILE, true);
    }

    private void init() {
        try {
            MANAGER.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (final IOException e) {
            ROOT.severe(e.getMessage());
        }
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void registry(final Logger logger, final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (IOException e) {
            ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGES_LOGGER.info(message);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGES_LOGGER.fine(message);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGES_LOGGER.info(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        ERRORS_LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }

}
